#!/usr/bin/env python3

print("Creating tuple of 100 elements by range function from 0 to 100 with 1 step: ")
tuple_1 = tuple(range(0, 101, 1))
for item in tuple_1:
    print("{} ".format(item), end="\n")
print("-" * 50)


print("Passing values from tuple to variables")
tuple_2 = tuple((0, 1, 2, 3, 4))
u, w, x, y, z = tuple_2
print(u, w, x, y, z)
