#!/usr/bin/env python3

print("List of three lists with one item: ")
list_1 = [[0], [0], [0]]
print(list_1)
print("-" * 50)


print("Add items to an empty list by appending: ")
list_2 = []
for num in range(100, 116):
    list_2.append(num)
print(list_2)
print("-" * 50)


print("Printing items of floats list: ")
list_3 = [2.29, 7.77869, 9.999, 59.01, 2.99]
for item in list_3:
    print("{} ".format(item), end="\n")
