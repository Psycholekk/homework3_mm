#!/usr/bin/env python3

from random import randrange

print("Let's find value 20!")
num: int = 0
while num != 20:
    num = randrange(0, 30)
    print(num, end="\n")
print("We found number {}".format(num))
print("-" * 50)


print("Let's iterate from 20 to 0!")
for num in range(20, -1, -1):
    print(num, end="\n")
