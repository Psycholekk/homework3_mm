#!/usr/bin/env python3

print("Creating dictionary with 5 items entered by user: ")
dict_1 = {}
for item in range(0, 5, 1):
    key: str = input("Please enter the key of element {}: ".format(item+1))
    dict_1[key] = input("Please enter the value of element {}: ".format(item+1))
print("Your dictionary is: ")
print(dict_1)
print("-" * 50)


print("Creating dictionary with 5 lists and items: ")
dict_2 = {"Flowers": ["Hibiscus", "Iris", "Lily", "Petunia", "Orchid"],
          "Animals": ["Dog", "Cat", "Hamster", "Rabbit", "Bird"],
          "Furniture": ["Table", "Chair", "Shelf", "Bed", "Stove"],
          "Names": ["Edward", "Jeff", "Bob", "Andy", "Denis"],
          "Dishes": ["Pizza", "Sandwich", "Sausages", "Schnitzel ", "Pierogi"]}
print(dict_2)
